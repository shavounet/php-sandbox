<?php


namespace VietnamTraining\Order\Repository;


use VietnamTraining\Order\Exception\CannotSaveOrderException;
use VietnamTraining\Order\OrderInterface;

interface OrderRepositoryInterface
{

    /**
     * @param OrderInterface $order
     * @throws CannotSaveOrderException
     */
    public function saveOrder(OrderInterface $order);

    public function findOrder(string $identifier): ?OrderInterface;
}
