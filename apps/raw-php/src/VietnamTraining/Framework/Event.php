<?php


namespace VietnamTraining\Framework;


use VietnamTraining\Order\OrderInterface;

class Event implements EventInterface
{
    /** @var string */
    protected $name;

    /** @var OrderInterface */
    protected $order;

    /** @var array */
    protected $additionalData;

    /**
     * Event constructor.
     *
     * @param string         $name
     * @param OrderInterface $order
     * @param array          $additionalData
     */
    public function __construct(string $name, OrderInterface $order, array $additionalData)
    {
        $this->name = $name;
        $this->order = $order;
        $this->additionalData = $additionalData;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return OrderInterface
     */
    public function getOrder(): OrderInterface
    {
        return $this->order;
    }

    /**
     * @return array
     */
    public function getAdditionalData(): array
    {
        return $this->additionalData;
    }
}
