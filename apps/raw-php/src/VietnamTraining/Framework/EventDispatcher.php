<?php


namespace VietnamTraining\Framework;


class EventDispatcher
{

    const EVENT_DO_SHIPPING = 'do_shipping';
    const EVENT_ORDER_SHIPPED = 'order_shipped';

    protected $listenersByEvent = [];

    /**
     * @param string                 $eventName
     * @param EventListenerInterface $listener
     */
    public function registerListener(string $eventName, EventListenerInterface $listener)
    {
        $this->listenersByEvent[$eventName][] = $listener;
    }

    /**
     * Dispatch an event and execute matching listener
     *
     * @param EventInterface $event
     */
    public function dispatch(EventInterface $event)
    {
        /** @var EventListenerInterface[] $listeners */
        $listeners = $this->listenersByEvent[$event->getName()];
        foreach ($listeners as $listener) {
            $listener->execute($event);
        }
    }

    /**
     * @return EventListenerInterface[]
     */
    public function getAllListeners(): array
    {
        return array_merge([], ...$this->listenersByEvent);
    }

    /**
     * @param string $eventName
     *
     * @return EventListenerInterface[]
     */
    public function getListeners(string $eventName): array
    {
        return $this->listenersByEvent[$eventName] ?? [];
    }

}
