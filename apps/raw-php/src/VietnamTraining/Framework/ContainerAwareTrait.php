<?php

namespace VietnamTraining\Framework;

trait ContainerAwareTrait
{
    public function getContainer(): Container
    {
        return $GLOBALS['APP_CONTAINER'];
    }
}
