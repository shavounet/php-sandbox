<?php


namespace VietnamTraining\Framework;


interface EventListenerInterface
{
    public function execute(EventInterface $event);
}
