<?php

use VietnamTraining\Framework\Container;
use VietnamTraining\Framework\EventDispatcher;

require_once __DIR__.'/vendor/autoload.php';

$container = new Container();
$GLOBALS['APP_CONTAINER'] = $container;


// Register your listeners here
//$container->getEventDispatcher()->registerListener(EventDispatcher::EVENT_DO_SHIPPING, $listener);


// Handle HTTP request
