<?php


use PHPUnit\Framework\TestCase;
use VietnamTraining\Order\Repository\ArrayOrderRepository;

class RepositoryTest extends TestCase
{

    public function testRepository()
    {
        $repo = new ArrayOrderRepository();
        $initOrder = new VietnamTraining\Order\Order(
            'VACLA-001',
            [91723],
            9.99,
            5.99,
            'new',
            'France'
        );
        $repo->saveOrder($initOrder);


        $initOrder->setStatus('any');

        $orderInRepo = $repo->findOrder($initOrder->getOrderReference());
        $this->assertEquals('new', $orderInRepo->getStatus());
    }
}
