<?php


use PHPUnit\Framework\TestCase;
use VietnamTraining\Framework\ContainerAwareTrait;
use VietnamTraining\Framework\Event;
use VietnamTraining\Framework\EventDispatcher;
use VietnamTraining\Order\OrderInterface;

class ListenerTest extends TestCase
{
    use ContainerAwareTrait;

    public function testShippingListeners()
    {
        $initOrder = new VietnamTraining\Order\Order('VACLA-001',[91723], 9.99, 5.99,'new', 'France');

        $listeners = $this->getContainer()->getEventDispatcher()->getListeners(EventDispatcher::EVENT_DO_SHIPPING);

        foreach ($listeners as $listener) {
            $order = clone $initOrder;
            $event = new Event(EventDispatcher::EVENT_DO_SHIPPING,$order , []);
            $listener->execute($event);
            self::assertEquals(OrderInterface::STATUS_SHIPPING, $order->getStatus());
        }
    }
}
