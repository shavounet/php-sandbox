<?php

use PHPUnit\Framework\TestCase;
use VietnamTraining\Framework\Container;
use VietnamTraining\Framework\ContainerAwareTrait;
use VietnamTraining\Framework\EventDispatcher;
use VietnamTraining\Framework\EventListenerInterface;

class ContainerTest extends TestCase
{
    use ContainerAwareTrait;

    public function testContainerInit()
    {
        self::assertTrue($this->getContainer() instanceof Container);
        self::assertTrue($this->getContainer()->getEventDispatcher() instanceof EventDispatcher);
    }

    public function testEventListenerTypes()
    {
        $listeners = $this->getContainer()->getEventDispatcher()->getAllListeners();

        foreach ($listeners as $listener) {
            self::assertTrue($listener instanceof EventListenerInterface);
        }
    }

}
