<?php


namespace Sandbox\MyModule\Manager;


class TrackManager implements TrackManagerInterface
{

    public $tracks = [
        [
            'name' => 'My Track !',
        ],
    ];

    /**
     * @param array $tracks
     */
    public function setTracks(array $tracks)
    {
        $this->tracks = $tracks;
    }


    /**
     * @return array
     */
    public function getTracks(): array
    {
        return $this->tracks;
    }

}
