<?php

namespace Sandbox\MyModule\Console\Command;

use Psr\Log\LoggerInterface;
use Sandbox\MyModule\Manager\TrackManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HelloWorldCommand extends Command
{

    /** @var LoggerInterface */
    protected $logger;

    /** @var TrackManagerInterface */
    protected $trackManager;

    /**
     * HelloWorldCommand constructor.
     *
     * @param LoggerInterface       $logger
     * @param TrackManagerInterface $trackManager
     */
    public function __construct(LoggerInterface $logger, TrackManagerInterface $trackManager)
    {
        $this->logger = $logger;
        $this->trackManager = $trackManager;

        parent::__construct();
    }


    protected function configure()
    {
        $this->setName('sandbox:hello-world');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Hello world');
        $output->writeln(print_r($this->trackManager->getTracks(), true));

        $this->logger->info('Hey it is working !');

        return 0;
    }


}
