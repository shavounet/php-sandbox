<?php


namespace Sandbox\MyModule\Model;


class SpotifyTrack implements TrackInterface
{
    /**
     * @return mixed
     */
    public function getData()
    {
        return [
            'API URL' => '...',
        ];
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function setData($data)
    {
        // TODO: Implement setData() method.
    }

    /**
     * @return mixed
     */
    public function play()
    {
        // TODO: Implement play() method.
    }

}
