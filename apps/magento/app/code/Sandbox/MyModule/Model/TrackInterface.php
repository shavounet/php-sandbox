<?php


namespace Sandbox\MyModule\Model;


interface TrackInterface
{
    public function getData();
    public function setData($data);
    public function play();


}
