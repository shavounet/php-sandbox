<?php


namespace Sandbox\MyModule\Block;


use Magento\Framework\View\Element\Template;

class MyContent extends Template
{
    public function getContentName()
    {
        return 'Content name';
    }

    public function isConfigOk()
    {
        // Do anything
        return false;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\ValidatorException
     */
    protected function _toHtml()
    {
        if($this->isConfigOk()) {
            return parent::_toHtml();
        }

        return '';
    }


}
